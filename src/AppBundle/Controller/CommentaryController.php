<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Commentary;
use AppBundle\Form\CommentaryType;

/**
 * Commentary controller.
 *
 * @Route("/")
 */
class CommentaryController extends Controller
{
    private function em() {
        return $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AppBundle:Commentary');
    }
    /**
     * Lists all Commentary entities.
     *
     * @Route("/", name="commentary")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $entities = $this->em()->getAllWithReplies();

        $entity = new Commentary();
        $form = $this->createCreateForm($entity);
        $replyForm = $this->createReplyForm(new Commentary);
        
        return [
            'entities' => $entities,
            'form' => $form->createView(),
            'replyForm' => $replyForm->createView()
        ];             
    }
    /**
     * Creates a new Commentary entity.
     *
     * @Route("/create", name="commentary_create")
     * @Method("POST")
     * @Template("AppBundle:Commentary:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Commentary();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->em()->create($entity);            
            return $this->redirect($this->generateUrl('commentary'));
        }

        return ['entity' => $entity, 'form' => $form->createView()];
    }
    
    /**
     * Creates reply;
     *
     * @Route("/create-reply/{id}", name="commentary_reply")
     * @Method("POST")
     */
    public function replyAction($id, Request $request)
    {
        $entity = new Commentary();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->em()->createReply($id, $entity);  

            return $this->redirect($this->generateUrl('commentary'));
        }

        return ['entity' => $entity, 'form'   => $form->createView()];
    }    

    /**
     * Creates a form to create a Commentary entity.
     *
     * @param Commentary $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Commentary $entity)
    {
        $form = $this->createForm(new CommentaryType(), $entity, [
            'action' => $this->generateUrl('commentary_create'),
            'method' => 'POST',
        ]);

        //$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Creates a form to reply a Commentary entity.
     *
     * @param Commentary $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createReplyForm(Commentary $entity)
    {
        $form = $this->createForm(new CommentaryType(), $entity, [
            'action' => $this->generateUrl('commentary_reply', array('id' => '__id__')),
            'method' => 'POST',
        ]);

        //$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }   

}
