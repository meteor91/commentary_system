<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Commentary;
use AppBundle\Form\CommentaryType;

/**
 * Description of CommentaryAdminController
 *
 * @author bilal
 */

/**
 * Commentary controller.
 * @Security("has_role('ROLE_ADMIN')")
 * @Route("/admin")
 */
class CommentaryAdminController extends Controller {
    private function em() {
        return $this->comments = $this->getDoctrine()
                    ->getManager()->getRepository('AppBundle:Commentary');
    }    
    /**
     * Lists all Commentary entities.
     *
     * @Route("/", name="admin_commentary")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $entities = $this->em()->getAllWithReplies();
        return ['entities' => $entities];        
    }
    
     /**
     * Deletes a Commentary entity.
     *
     * @Route("/commentary/{id}", name="admin_commentary_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->em()->delete($id);
        return $this->redirect($this->generateUrl('admin_commentary'));
    }
    
    
    /**
     * Displays a form to edit an existing Commentary entity.
     *
     * @Route("/{id}/edit", name="admin_commentary_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $entity = $this->em()->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Commentary entity.');
        }

        $editForm = $this->createEditForm($entity);
        return ['entity' => $entity, 'form' => $editForm->createView()];
    }    
    /**
     * Edits an existing Commentary entity.
     *
     * @Route("/commentary/{id}", name="commentary_update")
     * @Method("PUT")
     * @Template("AppBundle:Commentary:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Commentary')->find($id);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Commentary entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_commentary'));
        }

        return ['entity' => $entity, 'form' => $editForm->createView()];
    }
    /**
    * Creates a form to edit a Commentary entity.
    *
    * @param Commentary $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Commentary $entity)
    {
        $form = $this->createForm(new CommentaryType(), $entity, [
            'action' => $this->generateUrl('commentary_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ]);

        return $form;
    }    
 
}
