<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Comm;

class DefaultController extends Controller
{
    private function createTree() {
        $c1 = new Comm('level1');
        $c11 = new Comm('level11');
        $c111 = new Comm('level111');
        $c112 = new Comm('level112');
        $c1121 = new Comm('level1121');
        $c1122 = new Comm('level1122');
        $c1123 = new Comm('level1123');
        
        $c12 = new Comm('level12');

        $c1
        ->child($c11
            ->child($c111
                ->child(new Comm('level1111'))
                ->child(new Comm('level1112'))
            )
            ->child($c112
                ->child($c1121)
                ->child($c1122)
                ->child($c1123)
            )
        )->child($c12)
        ->child(new Comm('level13'));
        return $c1;
    }
    
    /**
     * @Route("/tree", name="tree")
     * @Template("AppBundle:lab:tree.html.twig")
     */
    public function indexAction()
    {

        return ['comm' => $this->createTree()];
    }
    
    /**
     * @Route("/tree-a", name="treea")
     * @Template("AppBundle:lab:tree_awesome.html.twig")
     */
    public function awesomeAction()
    {

        return ['comm' => $this->createTree()];
    }    
}
