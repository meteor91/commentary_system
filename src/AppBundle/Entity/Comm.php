<?php

namespace AppBundle\Entity;
/**
 * Description of Comm
 *
 * @author bilal
 */
class Comm {
    public function __construct($text) {
        $this->text = $text;
        //$this->childs = Array();
    }
    public $text;
    public $childs;
    public function child($child) {
        $this->childs[] = $child;
        return $this;
    }
}