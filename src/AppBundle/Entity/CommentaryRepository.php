<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Entity;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Commentary;

/**
 * Description of CommentaryRepository
 *
 * @author bilal
 */
class CommentaryRepository extends EntityRepository{
    public function getAllWithReplies() {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('comment, child')
            ->from('AppBundle:Commentary', 'comment')
            ->leftJoin('comment.children', 'child')
            ->where('comment.parent is NULL')
            ->orderBy('comment.createdAt', 'DESC');         

        $entities = $qb->getQuery()->execute();
        return $entities;
    }
    
    public function create(Commentary $commentary) {
        $em = $this->getEntityManager();
        $commentary->setText(nl2br($commentary->getText()));
        $em->persist($commentary);
        $em->flush();   
    }
    
    public function createReply($replyTo, Commentary $commentary) {
        $em = $this->getEntityManager();
        $commentary->setText(nl2br($commentary->getText()));
        $parent = $this->find($replyTo);
        $parent->addChild($commentary);
        $commentary->setParent($parent);
        $em->persist($commentary);
        $em->persist($parent);
        $em->flush();        
    }
    
    public function delete($id) {
        $em = $this->getEntityManager();
        $commentary = $this->find($id);
        if($commentary->getParent()!=null) {
            $parent = $this->find($commentary->getParent()->getId());
            $parent->getChildren()->removeElement($commentary);
            if (!$commentary) {
                throw $this->createNotFoundException('Unable to find Commentary entity.');
            }
            $em->persist($parent);
        }
        $em->remove($commentary);
        $em->flush();            
        
    }
}
